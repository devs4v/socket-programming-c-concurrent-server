#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h> 
//#include <stdio.h>
//#include <sys/types.h>
//#include <sys/socket.h>
#include <netdb.h>


int main(int argc, char *argv[])
{
  
  int simple_socket = 0;
  int simple_port = 0;
  int rc = 0;
  
  char buffer[256] = "";
  
  struct sockaddr_in TCP_Client;
  //struct TCP_Server;
  
  if (argc!= 3) 
    {
      fprintf(stderr, "Usage: %s <server> <port>\n", argv[0]);
      exit(1);
    }
  
  
  
  /* create a streaming socket */
  simple_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  if (simple_socket == -1)
    {
      fprintf(stderr, "Could not create a socket!\n");
      exit(1);
    } 
  
  else 
    {
      fprintf(stderr, "Socket created!\n");
    }
  /* retrieve the port number for connecting */
  simple_port = atoi(argv[2]);
  /* set up the address structure */
  /* use the IP address argument for the server address */
  bzero(&TCP_Client, sizeof(TCP_Client));
  
  TCP_Client.sin_family = AF_INET;
  TCP_Client.sin_port= htons(simple_port);

  /* connect to the address and port with our socket */
  rc = connect(simple_socket, (struct sockaddr *)&TCP_Client, sizeof(TCP_Client));
  if (rc == 0) {
    fprintf(stderr, "Connect successful!\n");
  }
  else {
    fprintf(stderr, "Could not connect to the address!\n");
    close(simple_socket);
    exit(1);
  }
  
  /* get the message from the server */
  rc = read(simple_socket, buffer, sizeof(buffer));
  if ( rc > 0 )
    {
      printf("%d: %s\n", rc, buffer);
    } 
  else 
    {
      fprintf(stderr, "Return Code = %d \n", rc);
    }
  close(simple_socket);
  return 0;
}






