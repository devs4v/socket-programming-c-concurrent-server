#include <stdio.h>
#include <unistd.h>  // defns of close(), write()
#include <string.h>  // defn of strlen
#include <strings.h>  // defn of bzero()
#include <sys/types.h>
#include <sys/socket.h>
#include <stdlib.h>  // defns of exit() etc
#include <netdb.h>
#include <signal.h>  // to define custom signal handler, otherwise Ctrl+C does not free socket

void exitHandler(int rc){
  exit(0);  // do a clean exit, frees sockets and ports
}


int main(char argc, char *argv[])
{
  
  // attach the handlers
  signal(SIGINT, exitHandler);
  signal(SIGTERM, exitHandler);
  
  
  char *x = "Hello from the server side!\n";
  
  //Declaring local variables
  
  int simple_socket = 0;       
  int simple_port = 0;
  int rc = 0;
  //struct sockaddr_in TCP_Server;
  
  struct sockaddr_in TCP_Server;
  
  if (argc != 2)
    {
      fprintf(stderr,"Usage:%s <port>\n",argv[0]);
      exit(1);
    }
  
  simple_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
  
  if(simple_socket == -1)
    {
      fprintf(stderr,"Cannot create a socket!\n");
      exit(1);	 
    } 
  
  else
    {
      fprintf(stderr,"Socket created!\n");   
    }
  
  // Getting the port number from the client
  
  simple_port = atoi(argv[1]);
  
  // Initializing address structure 
  
  bzero(&TCP_Server, sizeof(TCP_Server));
  
  TCP_Server.sin_family = AF_INET;
  TCP_Server.sin_addr.s_addr = htonl (INADDR_ANY);
  TCP_Server.sin_port = htons(simple_port);
  
  
  //  Binding the address and port with the created socket
  
  rc = bind(simple_socket, (struct sockaddr *)&TCP_Server, sizeof(TCP_Server));
  
  if(rc == 0)
    {
      fprintf(stderr, "Binding done!\n");
    }
  else
    {
      fprintf(stderr, "Cannot bind!\n");	 
      close(simple_socket);
      exit(1);
    }
  
  rc = listen(simple_socket, 3);
  if (rc>=0){
    fprintf(stderr, "Listening...\n");
  }else{
    fprintf(stderr, "Could not listen\n");
  }
  while(1)
    {
      struct sockaddr_in client_addr = {0};
      int simple_childSocket = 0;
      int client_addr_length = sizeof(client_addr);
      
      //Waiting for incoming connection from client
      
      simple_childSocket = accept(simple_socket,(struct sockaddr *)&client_addr, &client_addr_length);
      
      if (simple_childSocket == -1)
	{
	  fprintf(stderr, "Cannot accept connections!\n");
	  close(simple_socket);
	  exit(1);	  
	}
      
      // Send message/data to the client
      
      write(simple_childSocket, x, sizeof(x[0])*(strlen(x)+1));
      fprintf(stdout, "Sent message of %lu bytes to client\n",sizeof(x[0])*(strlen(x)+1));
      fflush(stdout);
      close(simple_childSocket);
      
    }	
  
  close(simple_socket);
  
  return 0;
  
}


